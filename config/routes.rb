Rails.application.routes.draw do
  resources :slices
  get '/ping/:uuid' => 'slices#ping'
  get '/cut/:uuid' => 'slices#cut', as: :cut
  put '/slot/:id/tag/:tag' => 'slices#tag', as: :tag
  get '/week/:uuid(/:week_no)' => 'slices#week', as: :week

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
