class CreateSlices < ActiveRecord::Migration[5.1]
  def change
    create_table :slices do |t|
      t.integer :timestamp
      t.string :user_hash
      t.text :desc
      t.integer :worth

      # t.timestamps
    end
  end
end
