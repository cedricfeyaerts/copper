class SlicesController < ApplicationController
  before_action :set_slice, only: [:show, :edit, :update, :destroy, :tag]

  # GET /slices
  # GET /slices.json
  def index
    @slices = Slice.all
  end

  # GET /slices/1
  # GET /slices/1.json
  def show
  end

  # GET /slices/new
  def new
    @slice = Slice.new
  end

  # GET /slices/1/edit
  def edit
  end

  # POST /slices
  # POST /slices.json
  def create
    @slice = Slice.new(slice_params)

    respond_to do |format|
      if @slice.save
        format.html { redirect_to @slice, notice: 'Slice was successfully created.' }
        format.json { render :show, status: :created, location: @slice }
      else
        format.html { render :new }
        format.json { render json: @slice.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /slices/1
  # PATCH/PUT /slices/1.json
  def update
    respond_to do |format|
      if @slice.update(slice_params)
        format.html { redirect_to @slice, notice: 'Slice was successfully updated.' }
        format.json { render :show, status: :ok, location: @slice }
      else
        format.html { render :edit }
        format.json { render json: @slice.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /slices/1
  # DELETE /slices/1.json
  def destroy
    @slice.destroy
    respond_to do |format|
      format.html { redirect_to slices_url, notice: 'Slice was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def ping
    Slice.ping(params[:uuid])
    render body: nil
  end

  def cut
    Slice.cut(params[:uuid])
    redirect_to week_path(params[:uuid])
  end

  def week
    @user_hash = params[:uuid]
    @active_week = params[:week_no].nil? || params[:week_no] == Time.zone.now.to_date.strftime("%U")
    Slice.ping(@user_hash) if params[:ping]
    @week_no = params[:week_no] || Time.zone.now.to_date.strftime("%U")
    now = Date.today
    monday = Date.commercial(now.cwyear,@week_no.to_i)
    @week_time = 0
    @week = (0..4).map do |d|
      date = (monday + d)
      timestamp = Slice.from_date(date).from_user(@user_hash).to_time
      @week_time += timestamp
      {
        date: date,
        slots: Slice.from_date(date).from_user(@user_hash).to_slots,
        time: Slice.human_time(timestamp),
        timestamp: Slice.human_time(timestamp)
      }
    end
    @week_time = Slice.human_time(@week_time)
  end

  def tag
    @slice.update(desc: params[:tag])
    user_hash = @slice.user_hash
    if params[:tag] == 'lunch'
      @slice.update(worth: 0)
      date = @slice.time.to_date
      slots = Slice.from_date(date).from_user(user_hash).where(worth: 0).where("id != ?", @slice.id).where("desc != ? OR desc IS NULL", 'lunch')
      # return render json: Slice.from_date(date).from_user(user_hash).where(worth: 0).where("id != ?", @slice.id).where("desc != ? OR desc is null", 'lunch').map(&:desc)
      slots.update_all(worth: 1, desc: 'Pipi break')
    end
    redirect_to week_path(@slice.user_hash)
  end



  private
    # Use callbacks to share common setup or constraints between actions.
    def set_slice
      @slice = Slice.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def slice_params
      params.require(:slice).permit(:time, :user_hash, :desc, :worth)
    end
end
