class Slice < ApplicationRecord

  TIME_PERIOD = 2.minute
  scope :from_date, ->(date){ where("timestamp >= ?", date.to_time.to_i).where("timestamp < ?", (date+1).to_time.to_i) }
  scope :from_user, ->(user_hash){ where(user_hash: user_hash) }
  scope :working, ->(worth = 1){where(worth: worth)}

  def time
    Time.zone.at(self.timestamp)
  end

  def time=(val)
    case val
    when String
      val = Time.parse(val)
    when Time
      val = val
    when Date
      val = val.to_time
    end
    self.timestamp = val.to_i
  end

  def self.ping(user_hash)
    time = Time.zone.now
    last_slice  = from_date(time.to_date).working.from_user(user_hash).order(timestamp: 'desc').first
    if last_slice.nil?
      #start_of_the_day
      create(time: time-1, user_hash: user_hash, worth:1, desc: 'start of the day')
      # first slice
      create(time: time, user_hash: user_hash, worth:1, desc: 'work')
    elsif last_slice.time > time - TIME_PERIOD
      last_slice.update(time: time)
    else
      create(time: time-1, user_hash: user_hash, worth: 0, desc: nil) unless last_slice.time < time.to_date
      create(time: time,  user_hash: user_hash, worth: 1, desc: 'work')
    end
  end

  def self.cut(user_hash)
    time = Time.zone.now
    last_slice  = from_date(time.to_date).working.from_user(user_hash).order(timestamp: 'desc').first
    create(time: time,  user_hash: user_hash, worth: last_slice.worth, desc: last_slice.desc )
  end

  def self.to_slots
    last_slice = nil
    slots = self.order(timestamp: :asc).map do |slice|
      if last_slice.nil?
        last_slice = slice
        next
      end
      time = slice.timestamp-last_slice.timestamp
      slot = {
        start: last_slice.time,
        close: slice.time,
        time: human_time(time),
        percent: percent(time),
        worth: slice.worth,
        id: slice.id,
        desc: slice.desc,
        title: "#{last_slice.time.strftime("%I:%M%p")} - #{slice.time.strftime("%I:%M%p")} <br> #{human_time(time)} -> #{slice.desc}"
      }
      last_slice = slice
      slot
    end

    slots.compact
  end

  def self.to_time
    all_slice = self.order(timestamp: :asc)
    return 0 unless all_slice.count > 2

    break_time = all_slice.where(worth: 0).map do |close_slice|
      start_slice = all_slice.where("timestamp < ?",close_slice.timestamp ).last
      close_slice.timestamp - start_slice.timestamp
    end
    break_time = break_time.inject(&:+)
    self.all.last.timestamp.to_i - self.all.first.timestamp.to_i - break_time.to_i
  end

  def self.percent(time)
    [time/432, 0.3].max
  end

  def self.human_time(sec)
    seconds = sec % 60
    minutes = (sec / 60) % 60
    hours = sec / (60 * 60)

    format("%02d:%02d:%02d", hours, minutes, seconds)
  end

end
