json.extract! slice, :id, :time, :user_hash, :desc, :worth, :created_at, :updated_at
json.url slice_url(slice, format: :json)
